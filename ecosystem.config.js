module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps: [
    {
      name: 'CGV_API',
      script: './bin/www',
      instances: 'max',
      exec_mode: 'cluster',
      watch: true,
      ignore_watch: ['node_modules', 'logs'],
      error_file: './logs/pm2/cluster_err.log',
      out_file: './logs/pm2/cluster_out.log',
      log_file: './logs/pm2/cluster_out_err.log',
      log_date_format: 'YYYY-MM-DD HH:mm Z',
    },
  ],
};