const express = require('express');
const router = express.Router();
const Movie = require('../app/movie/MovieController');

const movie = new Movie();
router.get('/movies', movie.movies);
router.get('/movie_detail', movie.detail);

module.exports = router;