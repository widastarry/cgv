const express = require('express');
const router = express.Router();
const Schedule = require('../app/schedule/ScheduleController');

const schedule = new Schedule();

router.get('/schedules', schedule.schedule);
router.get('/schedules_date', schedule.availableDate);
router.get('/schedules_area', schedule.cinemaArea);

module.exports = router;