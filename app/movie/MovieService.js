const request = require('request-promise');
const cheerio = require('cheerio');

class MovieService {

  constructor() {
    this.baseUrl = 'https://www.cgv.id';
    this.movieUri = '/en/movies/now_playing';
    this.detailUri = '/en/movies/detail/';
  }

  requestOptions(uri) {
    return {
      uri: this.baseUrl+uri,
      transform: (body) => {
        return cheerio.load(body);
      }
    }
  }

  listMovies() {
    const options = this.requestOptions(this.movieUri);
    return request(options)
      .then(($) => {
        const nowPlaying = [];
        $('.movie-list-body ul li').each((index, ul) => {
          const image = $(ul).find('img').attr('src');
          const detail = $(ul).find('.sel-body-hv .sel-body-play').attr('href');
          const arrDetail = detail.split('/');
          const id_movie = arrDetail[arrDetail.length - 1];

          nowPlaying.push({
            id_movie,
            image: this.baseUrl+image,
          });
        });


        const comingSoon = [];
        $('.comingsoon-movie-list-body ul li').each((index, ul) => {
          const image = $(ul).find('img').attr('src');
          const detail = $(ul).find('.sel-body-hv .sel-body-play').attr('href');
          const arrDetail = detail.split('/');
          const id_movie = arrDetail[arrDetail.length - 1];
          
          comingSoon.push({
            id_movie,
            image: this.baseUrl+image,
          });
        });

        return {
          now_playing: nowPlaying, 
          coming_soon: comingSoon,
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  detailMovie(idMovie) {
    const uri = `${this.detailUri}/${idMovie}`
    const options = this.requestOptions(uri);
    return request(options)
      .then(($) => {
        const title = $('.movie-info-title').text().replace(/\s\s+/g, ' ').trim();
        const image = $('.trailer-wrapper .poster-section img').attr('src');
        const video = $('.trailer-wrapper .trailer-section iframe').attr('src');
        const synopsis = $('.movie-synopsis').text().replace(/\s\s+/g, ' ');
        const descriptions = [];
        $('.movie-add-info > ul > li').each((index, li) => {
          const desc = $(li).text();
          descriptions.push(desc);
        });

        return {
          title,
          image: this.baseUrl+image,
          video,
          descriptions,
          synopsis,
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }
}

module.exports = MovieService;
