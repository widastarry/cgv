const MovieService = require('./MovieService.js');
const service = new MovieService();

class MovieController {

  movies(req, res, next) {
    return service.listMovies()
      .then((result) => {
        res.json(result);
      })
      .catch(console.log);
  }

  detail(req, res, next) {
    const idMovie = req.query.id_movie || '';
    if (idMovie.trim() === '') {
      return res.status(400).json({
        message: 'id_movie cannot be null',
      });
    }
    return service.detailMovie(idMovie)
      .then((result) => {
        res.json(result);
      })
      .catch(console.log);
  }
}

module.exports = MovieController;