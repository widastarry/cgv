const ScheduleService = require('./ScheduleService.js');
const service = new ScheduleService();

class ScheduleController {

  cinemaArea(req, res, next) {
    return service.cinemaArea()
      .then((result) => {
        return res.json(result);
      })
      .catch(console.log);
  }

  availableDate(req, res, next) {
    return service.availableDate()
      .then((result) => {
        return res.json(result);
      })
      .catch(console.log);
  }

  schedule(req, res, next) {
    const cinema_id = req.query.cinema_id || '';
    const date = req.query.date || '';
    if (cinema_id.trim() === '' || date.trim() === '') {
      return res.json({
        message: 'cinema_id and date cannot be null',
      });
    }
    return service.availableTypeAndTime(cinema_id, date)
      .then((result) => {
        if (typeof result == 'undefined') {
          return res.json({message : 'No movie'});
        }
        return res.json(result);
      })
      .catch(console.log)
  }

}

module.exports = ScheduleController;
