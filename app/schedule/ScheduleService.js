const request = require('request-promise');
const cheerio = require('cheerio');

class ScheduleService {

  constructor() {
    this.baseUrl = 'https://www.cgv.id';
    this.scheduleUri = '/en/schedule/cinema';
  }

  requestOptions(uri) {
    return {
      uri: this.baseUrl+uri,
      transform: (body) => {
        return cheerio.load(body);
      }
    }
  }

  cinemaArea() {
    const options = this.requestOptions(this.scheduleUri);
    return request(options)
      .then(($) => {
        const results = [];
        $('.sect-city ul li').each((index, city) => {
          const isCity = $(city).attr('class');
          if (isCity) {
            const cityClass = isCity.trim();
            if (cityClass === 'city' || cityClass === 'city on') {
              const cityName = $(city).find('.area').prev().text();
              const cinemas = [];
              const cinema = $(city).find('.area ul li');
              cinema.each((index, desc) => {
                const cinemaId = $(desc).find('a').attr('id');
                const cinemaPlace = $(desc, 'a').text();
                cinemas.push({
                  cinema_id: cinemaId,
                  cinema_place: cinemaPlace,
                });
              });

              results.push({
                city_name: cityName,
                cinemas,
              });
            }
          }
        });
        return results;
      })
      .catch(console.log);
  }

  availableDate() {
    const options = this.requestOptions(this.scheduleUri);
    return request(options)
      .then(($) => {
        const dates = [];
        $('.date-schedule ul li').each((index, li) => {
          const date = $(li).find('a').attr('date');
          dates.push(date);
        });
        return { dates };
      })
      .catch(console.log);
  }

  availableTypeAndTime(idMovie, date) {
    const uri = `${this.scheduleUri}/${idMovie}/${date}`;
    // const uri = `${this.scheduleUri}`;
    console.log(uri);
    const options = this.requestOptions(uri);
    return request(options)
      .then(($) => {
        const results = [];
        $('.schedule-lists > ul > li').each((index, li) => {
          const title = $(li).find('.schedule-title a').text();
          const description = $(li).find('.schedule-title span').text();
          const types = [];
          let iterator = 0;
          $(li).find('ul li').each((index, value) => {
            const type = {
              schedule_name: '',
              times: '',
            };
            if ($(value).hasClass('schedule-type')) {
              type.schedule_name = $(value).text().trim();
              types.push(type);
              iterator+=1;
            } else {
              const times = [];
              $(value).find('ul li').each((i, listTime) => {
                const notAvailable = $(listTime).find('a').hasClass('disabled');
                const time = $(listTime).text();
                let available = 'yes';
                if (notAvailable) available = 'no';
                times.push({time, available});
              });
              if (times.length != 0) types[iterator-1].times = times;
            }
          });
      
          results.push({
            title,
            description,
            types,
          });
        });
        return results;
      })
      .catch(console.log);    
  }

}

module.exports = ScheduleService;
